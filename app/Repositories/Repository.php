<?php
/**
 * Created by PhpStorm.
 * User: Kingsley
 * Date: 11/06/2018
 * Time: 10:35 PM
 */

namespace App\Repositories;
use Illuminate\Database\Eloquent\Model;


class Repository implements RepositoryInterface
{
    //Instantiate model properties on class instance
    protected $model;

    public function __construct(Model $model)
    {
        $this->model = $model;
    }

    public function all()
    {
        // TODO: Implement all() method.
        return $this->model->all();
    }

    public function create(array $data)
    {
        // TODO: Implement create() method.
        return $this->model->create($data);
    }

    public function update(array $data, $id)
    {
        // TODO: Implement update() method.
        $record = $this->findOrFail($id);
        return $record->update($data);
    }

    public function delete($id)
    {
        // TODO: Implement delete() method.
        return $this->model->findOrFail($id);
    }

    public function show($id)
    {
        // TODO: Implement show() method.
        return $this->model-findOrFail($id);
    }

    /**
     * @return Model
     */
    public function getModel(): Model
    {
        return $this->model;
    }

    /**
     * @param Model $model
     */
    public function setModel(Model $model)
    {
        $this->model = $model;
    }
}