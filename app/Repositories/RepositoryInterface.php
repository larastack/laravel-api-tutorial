<?php
/**
 * Created by PhpStorm.
 * User: Kingsley
 * Date: 11/06/2018
 * Time: 10:37 PM
 */

namespace App\Repositories;


interface RepositoryInterface
{
    public function all();
    public function create(array $data);
    public function update(array $data, $id);
    public function delete($id);
    public function show($id);

}