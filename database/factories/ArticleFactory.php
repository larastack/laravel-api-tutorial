<?php
//To create a factory you use php artisan make:factory ArticleFactory
use Faker\Generator as Faker;

$factory->define(App\Article::class, function (Faker $faker) {
    return [
        'title' => $faker->text(50),
        'body' => $faker->text(255)
    ];
});
